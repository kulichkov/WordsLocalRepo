import XCTest
@testable import WordsLocalRepo

final class WordsLocalRepoTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(WordsLocalRepo().text, "Hello, World!")
    }
}
