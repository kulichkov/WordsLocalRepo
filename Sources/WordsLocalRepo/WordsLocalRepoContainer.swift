//
//  WordsLocalRepoContainer.swift
//  
//
//  Created by Mikhail Kulichkov on 29.06.22.
//

import Foundation
import CoreData

open class WordsLocalRepoContainer: NSPersistentContainer {

    override open class func defaultDirectoryURL() -> URL {

        return super.defaultDirectoryURL()
            .appendingPathComponent("CoreDataModel")
            .appendingPathComponent("Local")
    }
}
