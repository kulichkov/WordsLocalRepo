//
//  File.swift
//  
//
//  Created by Mikhail Kulichkov on 29.06.22.
//

import Foundation
import CoreData

public class SolidWordsLocalRepoManager: WordsLocalRepoManager {

    public var viewContext: NSManagedObjectContext { container.viewContext }

    var container: WordsLocalRepoContainer

    required public init(
        configuration: WordsLocalRepoConfiguration
    ) {

        let model = SolidWordsLocalRepoManager.model(for: configuration.modelName)

        self.container = .init(name: configuration.modelName,
                               managedObjectModel: model)

        self.container.persistentStoreDescriptions
            .first?
            .configuration = configuration.configuration

        self.container.persistentStoreDescriptions
            .first?
            .type = NSInMemoryStoreType

        self.container.loadPersistentStores(completionHandler: { (desc, err) in

            if let err = err {

                fatalError("Error loading SOLID STORE: \(desc): \(err)")
            }

            debugPrint("Loaded SOLID STORE successfully")
        })
    }

}
