//
//  WordsLocalRepoManager.swift
//  
//
//  Created by Mikhail Kulichkov on 29.06.22.
//

import Foundation
import CoreData

public protocol WordsLocalRepoManager {

    var viewContext: NSManagedObjectContext { get }

    init(configuration: WordsLocalRepoConfiguration)
}

extension WordsLocalRepoManager {

    static func model(for name: String) -> NSManagedObjectModel {

        guard let url = Bundle.module.url(forResource: name, withExtension: "mom") else { fatalError("Could not get URL for model: \(name)") }

        guard let model = NSManagedObjectModel(contentsOf: url) else { fatalError("Could not get model for: \(url)") }

        return model
    }
}
