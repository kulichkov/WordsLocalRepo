//
//  WordsLocalRepoConfiguration.swift
//  
//
//  Created by Mikhail Kulichkov on 29.06.22.
//

import Foundation

public struct WordsLocalRepoConfiguration {

    public init(
        modelName: String,
        configuration: String
    ) {

        self.modelName = modelName
        self.configuration = configuration
    }

    public let modelName: String
    public let configuration: String
}
